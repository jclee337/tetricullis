import java.awt.Point;

public class Block {
  public int x, y;
  private Texture texture;
  
  public Block(Point position, Texture texture) {
    this.x = position.x;
    this.y = position.y;
    this.texture = texture;
  }
  
  public void translate(int x, int y) {
    this.x += x;
    this.y += y;
  }
  
  public boolean equals(Block block) {
    return block.x == x && block.y == y;
  }
  
  public void draw(Point fieldPosition, int cellSize) {
    Graphics.getInstance().drawTexture(new Point(
      (int)(fieldPosition.x + x * cellSize), 
      (int)(fieldPosition.y + y * cellSize)), 0, 1, 1, texture);
  }
}
