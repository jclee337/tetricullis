import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;

import org.lwjgl.opengl.Display;
import org.lwjgl.util.Color;

public class PlayField {
  private Point position;
  private Point dimensions;
  private Rectangle nextWindow, holdWindow, scoreWindow, statWindow;
  private int cellSize, score, multiplier, linesCleared, drops, totalTetrominos;
  private double sumTime, playTime, fallDelay, lockDelay;
  private Tetromino control, hold;
  private ArrayList<Tetromino> pool;
  private ArrayList<Tetromino> queue;
  private ArrayList<Block> active;
  private boolean ghostGrid, gameOver, lockHold;
  
  public PlayField(ArrayList<Tetromino> pool) {
    position = new Point(0, 0);
    dimensions = new Point(10, 22);
    cellSize = 24;
    score = 0;
    multiplier = 1;
    linesCleared = 0;
    drops = 0;
    totalTetrominos = 1;
    sumTime = 0;
    fallDelay = 1;
    lockDelay = 1;
    nextWindow = new Rectangle(Display.getWidth() / 7 - 60, 3 * Display.getHeight() / 4 - 50, 130, 130);
    holdWindow = new Rectangle(Display.getWidth() / 7 - 60, Display.getHeight() / 4 - 50, 130, 130);
    scoreWindow = new Rectangle(6 * Display.getWidth() / 7 - 70, Display.getHeight() / 4 - 50, 130, 130);
    statWindow = new Rectangle(6 * Display.getWidth() / 7 - 70, 3 * Display.getHeight() / 4 - 50, 130, 130);
    this.pool = pool;
    queue = new ArrayList<Tetromino>();
    active = new ArrayList<Block>();
    fillQueue();
    for(int i = 0; i < queue.size(); i++) {
      if(queue.get(i).getType() == Tetromino.Type.S || queue.get(i).getType() == Tetromino.Type.Z) {
        queue.remove(i);
        i--;
      }
    }
    ghostGrid = true;
    gameOver = false;
    lockHold = false;
  }
  
  public void update(long deltaTime) {
    if(!gameOver) {
      double seconds = deltaTime / 1e9;
      sumTime += seconds;
      playTime += seconds;
      
      if(queue.size() <= 3)
        fillQueue();
      
      if(control == null) {
        control = queue.remove(0);
        control.setPosition(new Point(dimensions.x / 2 - (int)Math.ceil(control.getSize() / 2.0), 0));
        totalTetrominos++;
      }
      
      if(hold == null){
        hold = queue.remove(0);
      }
      
      if(sumTime > fallDelay && control.canFall(active)) {
        drops++;
        control.incPosition(new Point(0, 1));
        sumTime = 0;
      } else if(sumTime > lockDelay && !control.canFall(active)) {
        // switch control piece
        drops++;
        totalTetrominos++;
        control.setLocked(true);
        active.addAll(control.getBlocks());
        checkRows();
        control = queue.remove(0);
        control.setLocked(false);
        control.setPosition(new Point(dimensions.x / 2 - (int)Math.ceil(control.getSize() / 2.0), 0));
        sumTime = 0;
      }
      
      // ghost shows how the control piece will eventually land
      Tetromino ghost = control.clone();
      ghost.hardDrop(active);
      Graphics.getInstance().setColor(new Color(255, 255, 255, 50));
      ghost.draw(position, cellSize);
      Graphics.getInstance().setColor(null);
    }
  }
  
  public void checkRows() {
    Block b = new Block(new Point(0, 0), null);
    int cleared = 0;
    for(int i = 0; i < dimensions.y; i++) {
      int tally = 0;
      for(int j = 0; j < dimensions.x; j++) {
        b.x = j;
        b.y = i;
        for(int k = 0; k < active.size(); k++) {
          if(active.get(k).equals(b))
            tally++;
          if(active.get(k).y <= 1)
            gameOver = true;
        }
      }
      if(tally >= 10) {
        clearRow(i);
        cleared++;
      }
    }
    score += Math.floor(Math.pow(cleared, 1.7)) * multiplier;
    linesCleared += cleared;
    
    if(multiplier == 1)
      multiplier = cleared;
    else
      multiplier += cleared;
    
    if(cleared == 0)
      multiplier = 1;
    
    lockHold = false;
  }
  
  public void clearRow(int index) {
    for(int i = 0; i < active.size(); i++) {
      if(active.get(i).y == index) {
        active.remove(i);
        i--;
      } else if(active.get(i).y < index) {
        active.get(i).y++;
      }
    }
  }
  
  public void fillQueue() {
    ArrayList<Tetromino> bag = new ArrayList<Tetromino>();
    for(int i = 0; i < pool.size(); i++)
      bag.add(pool.get(i).clone());
    while(!bag.isEmpty())
      queue.add(bag.remove((int)(Math.random() * bag.size())));
  }
  
  public void hold() {
    if(!lockHold) {
      lockHold = true;
      Tetromino t = control.clone();
      t.setPosition(new Point(0, 0));
      control = hold.clone();
      control.setLocked(false);
      control.setPosition(new Point(dimensions.x / 2 - (int)Math.ceil(control.getSize() / 2.0), 0));
      hold = t;
    }
  }
  
  public boolean collides(Tetromino tetromino) {
    ArrayList<Block> blocks = tetromino.getBlocks();
    for(int i = 0; i < blocks.size(); i++) {
      for(int j = 0; j < active.size(); j++) {
        if(active.get(j).equals(blocks.get(i)))
          return true;
      }
      if(blocks.get(i).x >= dimensions.x || blocks.get(i).y >= dimensions.y || blocks.get(i).x < 0 || blocks.get(i).y < 0)
        return true;
    }
    return false;
  }
  
  public void setPosition(Point position) {
    this.position = position;
  }
  
  public Point getPosition() {
    return position;
  }
  
  public void setDimensions(Point dimensions) {
    this.dimensions = dimensions;
  }
  
  public Point getDimensions() {
    return dimensions;
  }
  
  public void setCellSize(int cellSize) {
    this.cellSize = cellSize;
  }
  
  public void skipFrame() {
    sumTime = fallDelay;
  }
  
  public int getCellSize() {
    return cellSize;
  }
  
  public void setFallDelay(double fallDelay) {
    this.fallDelay = fallDelay;
  }
  
  public void addPool(Tetromino piece) {
    pool.add(piece);
  }
  
  public void setControl(Tetromino control) {
    this.control = control;
  }
  
  public Tetromino getControl() {
    return control;
  }
  
  public ArrayList<Block> getActive() {
    return active;
  }
  
  public void draw() {
    if(ghostGrid) {
      Graphics.getInstance().setColor(new Color(255, 255, 255, 50));
      for(int i = 1; i < dimensions.x; i++)
        Graphics.getInstance().drawLine(new Point(position.x + cellSize * i, position.y), new Point(position.x + cellSize * i, position.y + cellSize * dimensions.y));
      
      for(int j = 1; j < dimensions.y; j++)
        Graphics.getInstance().drawLine(new Point(position.x, position.y + cellSize * j), new Point(position.x + cellSize * dimensions.x, position.y + cellSize * j));
      
      Graphics.getInstance().setColor(null);
    }
    
    Graphics.getInstance().setFillDrawn(false);
    Graphics.getInstance().setColor(new Color(Color.CYAN));
    Graphics.getInstance().drawQuad(position.x - 1, position.y - 1, dimensions.x * cellSize + 1, dimensions.y * cellSize + 2);
    Graphics.getInstance().drawQuad(position.x - 4, position.y - 4, dimensions.x * cellSize + 7, dimensions.y * cellSize + 8);
    Graphics.getInstance().drawQuad(nextWindow.getX(), nextWindow.getY(), nextWindow.getWidth(), nextWindow.getHeight());
    Graphics.getInstance().drawQuad(nextWindow.getX() - 3, nextWindow.getY() - 4, nextWindow.getWidth() + 6, nextWindow.getHeight() + 7);
    Graphics.getInstance().drawQuad(holdWindow.getX(), holdWindow.getY(), holdWindow.getWidth(), holdWindow.getHeight());
    Graphics.getInstance().drawQuad(holdWindow.getX() - 3, holdWindow.getY() - 4, holdWindow.getWidth() + 6, holdWindow.getHeight() + 7);
    Graphics.getInstance().drawQuad(scoreWindow.getX(), scoreWindow.getY(), scoreWindow.getWidth(), scoreWindow.getHeight());
    Graphics.getInstance().drawQuad(scoreWindow.getX() - 4, scoreWindow.getY() - 4, scoreWindow.getWidth() + 6, scoreWindow.getHeight() + 6);
    Graphics.getInstance().drawQuad(statWindow.getX(), statWindow.getY(), statWindow.getWidth(), statWindow.getHeight());
    Graphics.getInstance().drawQuad(statWindow.getX() - 4, statWindow.getY() - 4, statWindow.getWidth() + 6, statWindow.getHeight() + 6);
    Graphics.getInstance().setColor(null);
    Graphics.getInstance().setFillDrawn(true);
    
    Graphics.getInstance().drawString(new Point((int)nextWindow.getCenterX(), (int)nextWindow.getY() - 20), 0, true, "Next");
    //queue.get(0).draw(new Point((int)(nextWindow.getCenterX() - queue.get(0).getSize() * 22 / 2), (int)(nextWindow.getCenterY() - 25)), cellSize);
    queue.get(0).draw(new Point((int)(nextWindow.getCenterX() - queue.get(0).getSize() * 22 / 2), (int)(nextWindow.getCenterY() - queue.get(0).getSize() * 24 / 2)), cellSize);
    
    Graphics.getInstance().drawString(new Point((int)holdWindow.getCenterX(), (int)holdWindow.getY() - 20), 0, true, "Hold");
    hold.draw(new Point((int)holdWindow.getCenterX() - hold.getSize() * 22 / 2, (int)holdWindow.getCenterY() - 25), cellSize);
    
    Graphics.getInstance().drawString(new Point((int)scoreWindow.getCenterX(), (int)(scoreWindow.getY() - 20)), 0, true, "Score");
    Graphics.getInstance().drawString(new Point((int)scoreWindow.getCenterX(), (int)scoreWindow.getCenterY()), 0, true, Integer.toString(score));
    Graphics.getInstance().drawString(new Point((int)scoreWindow.getCenterX(), (int) scoreWindow.getMaxY() + 20), 0, true, 
        "Multiplier: x" + Integer.toString(multiplier));
    
    Graphics.getInstance().drawString(new Point((int)statWindow.getCenterX(), (int)(statWindow.getY() - 20)), 0, true, "Stats");
    Graphics.getInstance().drawString(new Point((int)statWindow.getCenterX(), (int)statWindow.getY() + 25), 0, true, 
        "Lines: " + Integer.toString(linesCleared));
    Graphics.getInstance().drawString(new Point((int)statWindow.getCenterX(), (int)statWindow.getY() + 50), 0, true, 
        "Pieces: " + Integer.toString(totalTetrominos));
    Graphics.getInstance().drawString(new Point((int)statWindow.getCenterX(), (int)statWindow.getY() + 75), 0, true,
        "Time: " + Integer.toString((int)playTime));
    Graphics.getInstance().drawString(new Point((int)statWindow.getCenterX(), (int)statWindow.getY() + 100), 0, true,
        "Drops: " + Integer.toString(drops));
    
    if(!gameOver)
      control.draw(position, cellSize);
    
    for(int i = 0; i < active.size(); i++)
      active.get(i).draw(position, cellSize);
  }
}
