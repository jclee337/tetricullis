import static org.lwjgl.opengl.GL11.GL_COLOR_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_DEPTH_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_DEPTH_TEST;
import static org.lwjgl.opengl.GL11.GL_MODELVIEW;
import static org.lwjgl.opengl.GL11.GL_PROJECTION;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.glClear;
import static org.lwjgl.opengl.GL11.glDisable;
import static org.lwjgl.opengl.GL11.glEnable;
import static org.lwjgl.opengl.GL11.glLoadIdentity;
import static org.lwjgl.opengl.GL11.glMatrixMode;
import static org.lwjgl.opengl.GL11.glOrtho;
import static org.lwjgl.opengl.GL11.glShadeModel;
import static org.lwjgl.opengl.GL11.glViewport;

import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;

import javax.imageio.ImageIO;

import org.lwjgl.LWJGLException;
import org.lwjgl.Sys;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;


public class Main {
  private State state;
  private int width = 640;
  private int height = 640;
  private long deltaTime; // the amount of time that has passed since the last tick
  private long fps;       // however many ticks (frames) happen per second
  private int score;
  private Graphics g;
  private PlayField field;
  private boolean oneShot = true;
  
  public static void main(String[] args) {
    new Main();
  }
  
  public Main() {
    try {
      Display.setDisplayMode(new DisplayMode(width, height));
      Display.setTitle("Tetricullis");
      ByteBuffer[] list = new ByteBuffer[2];
      try {
        list[0] = loadIcon("images/icon16.png", 16, 16);
        list[1] = loadIcon("images/icon32.png", 32, 32);
      }
      catch (IOException e) { e.printStackTrace(); }
      Display.setIcon(list);
      Display.create();
      
      // Controller controller = ControllerEnvironment.getDefaultEnvironment.getControllers();

      // enable textures since we're going to use these for our sprites
      glEnable(GL_TEXTURE_2D);
      
      glShadeModel(GL11.GL_SMOOTH); 
      
      // enable transparency
      glEnable(GL11.GL_BLEND);
      GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);

      // disable the OpenGL depth test since we're rendering 2D graphics
      glDisable(GL_DEPTH_TEST);

      glMatrixMode(GL_PROJECTION);
      glLoadIdentity();

      glOrtho(0, width, height, 0, -1, 1);
      glMatrixMode(GL_MODELVIEW);
      glLoadIdentity();
      glViewport(0, 0, width, height);
    } 
    catch (LWJGLException e) { e.printStackTrace(); }
    
    state = State.PLAY;
    g = Graphics.getInstance();
    
    Tetromino i = new Tetromino(new int[][] {
      {0, 0, 0, 0},
      {1, 1, 1, 1},
      {0, 0, 0, 0},
      {0, 0, 0, 0},
    }, Tetromino.Type.I, "images/I2.png");
    
    Tetromino j = new Tetromino(new int[][] {
      {1, 0, 0},
      {1, 1, 1},
      {0, 0, 0},
    }, Tetromino.Type.J, "images/J2.png");
    
    Tetromino l = new Tetromino(new int[][] {
      {0, 0, 1},
      {1, 1, 1},
      {0, 0, 0},
    }, Tetromino.Type.L, "images/L2.png");
    
    Tetromino o = new Tetromino(new int[][] {
      {1, 1},
      {1, 1},
    }, Tetromino.Type.O, "images/O2.png");
    
    Tetromino s = new Tetromino(new int[][] {
      {0, 1, 1},
      {1, 1, 0},
      {0, 0, 0},
    }, Tetromino.Type.S, "images/S2.png");
    
    Tetromino t = new Tetromino(new int[][] {
      {0, 1, 0},
      {1, 1, 1},
      {0, 0, 0},
    }, Tetromino.Type.T, "images/T2.png");
    
    Tetromino z = new Tetromino(new int[][] {
      {1, 1, 0},
      {0, 1, 1},
      {0, 0, 0},
    }, Tetromino.Type.Z, "images/Z2.png");
    
    ArrayList<Tetromino> pool = new ArrayList<Tetromino>();
    pool.add(i);
    pool.add(j);
    pool.add(l);
    pool.add(o);
    pool.add(s);
    pool.add(t);
    pool.add(z);
    
    field = new PlayField(pool);
    field.setPosition(new Point(Display.getWidth() / 2 - field.getDimensions().x * field.getCellSize() / 2, 
      Display.getHeight() / 2 - field.getDimensions().y * field.getCellSize() / 2));
    
    run();
  }
  
  public void run() {
    double start, end, totalTime = 0;
    deltaTime = 1;
    int ticks = 0;
    while(!Display.isCloseRequested()) {
      start = (Sys.getTime() * 1e9) / Sys.getTimerResolution();
      update();
      end = (Sys.getTime() * 1e9) / Sys.getTimerResolution();
      ticks++;
      // 1e6 nanoseconds in 1 millisecond
      deltaTime = (long)(end - start);
      totalTime += deltaTime;
      if(totalTime >= 1e9) { // 1e9 nanoseconds in 1 second
        fps = ticks;
        ticks = 0;
        totalTime -= 1e9;
      }
    }
    Display.destroy();
  }
  
  public void update() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    Display.sync(60);
    
    if(state == State.PLAY) {
      input();
      field.update(deltaTime);
      field.draw();
    }
    
    else if(state == State.PAUSE)
      pause();
    
    Display.update();
  }
  
  public void pause()
  {
    if((Keyboard.isKeyDown(Keyboard.KEY_ESCAPE) || Keyboard.isKeyDown(Keyboard.KEY_P)) && oneShot)
    {
      oneShot = false;
      state = State.PLAY;
    }
    
    if(!Keyboard.isKeyDown(Keyboard.KEY_ESCAPE) && !Keyboard.isKeyDown(Keyboard.KEY_P))
      oneShot = true;
    
    Graphics.getInstance().drawString(new Point(Display.getWidth() / 2, Display.getHeight() / 2), 0, true, "Paused");
    Graphics.getInstance().drawString(new Point(Display.getWidth() / 2, Display.getHeight() / 2), 0, true, "\n\n(Scrub Mode Engaged)");
  }
  
  public void input()
  {
    if(Keyboard.isKeyDown(Keyboard.KEY_Z) && oneShot)
    {
      oneShot = false;
      field.getControl().rotate(true);
      if(field.collides(field.getControl()))
      {
        field.getControl().incPosition(new Point(1, 0));
        if(field.collides(field.getControl()))
        {
          field.getControl().incPosition(new Point(-2, 0));
          if(field.collides(field.getControl()))
          {
            field.getControl().incPosition(new Point(1, 0));
            field.getControl().rotate(false);
          }
        }
      }
    }
    else if(Keyboard.isKeyDown(Keyboard.KEY_X) && oneShot)
    {
      oneShot = false;
      field.getControl().rotate(false);
      if(field.collides(field.getControl()))
      {
        field.getControl().incPosition(new Point(1, 0));
        if(field.collides(field.getControl()))
        {
          field.getControl().incPosition(new Point(-2, 0));
          if(field.collides(field.getControl()))
          {
            field.getControl().incPosition(new Point(1, 0));
            field.getControl().rotate(true);
          }
        }
      }
    }
    if(Keyboard.isKeyDown(Keyboard.KEY_RIGHT) && oneShot)
    {
      oneShot = false;
      if(!(field.getControl().getRightMax() >= 9))
      {
        field.getControl().incPosition(new Point(1, 0));
        if(field.collides(field.getControl()))
          field.getControl().incPosition(new Point(-1, 0));
      }
    }
    else if(Keyboard.isKeyDown(Keyboard.KEY_LEFT) && oneShot)
    {
      oneShot = false;
      if(!(field.getControl().getLeftMax() <= 0))
      {
        field.getControl().incPosition(new Point(-1, 0));
        if(field.collides(field.getControl()))
          field.getControl().incPosition(new Point(1, 0));
      }
    }
    if(Keyboard.isKeyDown(Keyboard.KEY_DOWN))
    {
      field.setFallDelay(0.05);
    }
    else
    {
      field.setFallDelay(1);
    }
    if(Keyboard.isKeyDown(Keyboard.KEY_UP) && oneShot)
    {
      oneShot = false;
      field.getControl().hardDrop(field.getActive());
      field.skipFrame();
    }
    
    if(Keyboard.isKeyDown(Keyboard.KEY_SPACE))
      field.hold();
    
    if((Keyboard.isKeyDown(Keyboard.KEY_ESCAPE) || Keyboard.isKeyDown(Keyboard.KEY_P)) && oneShot)
    {
      oneShot = false;
      state = State.PAUSE;
    }
    
    if(!Keyboard.isKeyDown(Keyboard.KEY_X) && !Keyboard.isKeyDown(Keyboard.KEY_Z) 
        && !Keyboard.isKeyDown(Keyboard.KEY_RIGHT) && !Keyboard.isKeyDown(Keyboard.KEY_LEFT) 
        && !Keyboard.isKeyDown(Keyboard.KEY_UP) && !Keyboard.isKeyDown(Keyboard.KEY_ESCAPE) && !Keyboard.isKeyDown(Keyboard.KEY_P))
    {
      oneShot = true;
    }
  }
  
  public ByteBuffer loadIcon(String filename, int width, int height) throws IOException 
  {
      BufferedImage image = ImageIO.read(getClass().getClassLoader().getResource(filename)); // load image
      // convert image to byte array
      byte[] imageBytes = new byte[width * height * 4];
      for (int i = 0; i < height; i++) 
      {
          for (int j = 0; j < width; j++) 
          {
              int pixel = image.getRGB(j, i);
              for (int k = 0; k < 3; k++) // red, green, blue
                  imageBytes[(i*16+j)*4 + k] = (byte)(((pixel>>(2-k)*8))&255);
              imageBytes[(i*16+j)*4 + 3] = (byte)(((pixel>>(3)*8))&255); // alpha
          }
      }
      return ByteBuffer.wrap(imageBytes);
  }
  
  public enum State
  {
    TITLE, PLAY, PAUSE
  }
}
