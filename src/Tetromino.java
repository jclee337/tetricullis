import java.awt.Point;
import java.io.IOException;
import java.util.ArrayList;

public class Tetromino {
  private int[][] map;
  private ArrayList<Block> blocks;
  private int width, height, size;
  private Type type;
  private Texture texture;
  private String filename;
  private Point position;
  private boolean locked, drawGhost;
  
  public Tetromino(int[][] map, Type type, String filename) {
    this.map = map;
    this.type = type;
    locked = false;
    drawGhost = false;
    this.size = map.length;
    width = size;
    height = size;
    this.filename = filename;
    loadTexture(filename);
    position = new Point(0, 0);
    remap();
  }
  
  private void remap() {
    blocks = new ArrayList<Block>();
    for(int i = 0; i < width; i++)
      for(int j = 0; j < height; j++)
        if(map[j][i] == 1)
          blocks.add(new Block(new Point(position.x + i, position.y + j), texture));
  }
  
  public void rotate(boolean clockwise) {
    int[][] newMap = new int[size][size];
    double x, y;
    for(int i = 0; i < size; i++) {
      for(int j = 0; j < size; j++) {
        if(clockwise) {
          x = size - j - 1;
          y = i;
        } else {
          x = j;
          y = size - i - 1;
        }
        newMap[(int)x][(int)y] = map[i][j];
      }
    }
    if(!locked) {
      map = newMap;
      remap();
    }
  }
  
  public Type getType() {
    return type;
  }
  
  public void hardDrop(ArrayList<Block> active) {
    while(canFall(active))
      incPosition(new Point(0, 1));
    locked = true;
  }
  
  public void incPosition(Point increment) {
    this.position.translate(increment.x, increment.y);
    for(int i = 0; i < blocks.size(); i++)
      blocks.get(i).translate(increment.x, increment.y);
  }
  
  public void setPosition(Point position) {
    this.position = position;
    remap();
  }
  
  public Point getPosition() {
    return position;
  }
  
  public int getSize() {
    return size;
  }
  
  public int getLeftMax() {
    int max = Integer.MAX_VALUE;
    for(int i = 0; i < blocks.size(); i++) {
      if(blocks.get(i).x < max)
        max = blocks.get(i).x;
    }
    return max;
  }
  
  public int getRightMax() {
    int max = 0;
    for(int i = 0; i < blocks.size(); i++) {
      if(blocks.get(i).x > max)
        max = blocks.get(i).x;
    }
    return max;
  }
  
  public int getBottomMax() {
    int max = 0;
    for(int i = 0; i < blocks.size(); i++) {
      if(blocks.get(i).y > max)
        max = blocks.get(i).y;
    }
    return max;
  }
  
  public void setLocked(boolean locked) {
    this.locked = locked;
  }
  
  public boolean isLocked() {
    return locked;
  }
  
  public boolean canFall(ArrayList<Block> active) {
    for(int i = 0; i < active.size(); i++) {
      for(int j = 0; j < blocks.size(); j++) {
        if(new Block(new Point(blocks.get(j).x, blocks.get(j).y + 1), texture).equals(active.get(i)))
          return false;
      }
    }
    if(getBottomMax() >= 21)
      return false;
    return true;
  }
  
  
  
  public boolean containsBlock(Block block) {
    for(int i = 0; i < blocks.size(); i++)
      if(blocks.get(i).equals(block))
        return true;
    return false;
  }
  
  public ArrayList<Block> getBlocks() {
    return blocks;
  }
  
  public void deleteRow(int index) {
    int[][] newMap = new int[width][height - 1];
    int newCounter = 0;
    for(int i = 0; i < height; i++) {
      if(i != index) {
        for(int j = 0; j < width; j++)
          newMap[j][newCounter] = map[j][i];
        newCounter++;
      }
    }
    map = newMap;
    remap();
  }
  
  public void deleteColumn() {
    // TODO
  }
  
  public void draw(Point fieldPosition, int cellSize) {
    for(int i = 0; i < blocks.size(); i++)
      Graphics.getInstance().drawTexture(new Point(
        (int)(fieldPosition.x + blocks.get(i).x * cellSize), 
        (int)(fieldPosition.y + blocks.get(i).y * cellSize)), 0, 1, 1, texture);
  }
  
  public void loadTexture(String filename) {
    try {
      texture = Graphics.getInstance().getTextureLoader().getTexture(filename);
    } 
    catch (IOException e) { e.printStackTrace(); }
  }
  
  public Tetromino clone() {
    Tetromino clone = new Tetromino(map, type, filename);
    clone.setLocked(Boolean.valueOf(locked));
    clone.setPosition(new Point(position.x, position.y));
    clone.remap();
    return clone;
  }
  
  public static enum Type {
    I, J, L, O, S, T, Z
  }
}
