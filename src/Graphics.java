
import static org.lwjgl.opengl.GL11.GL_FILL;
import static org.lwjgl.opengl.GL11.GL_FRONT;
import static org.lwjgl.opengl.GL11.GL_FRONT_AND_BACK;
import static org.lwjgl.opengl.GL11.GL_LINE;
import static org.lwjgl.opengl.GL11.GL_LINES;
import static org.lwjgl.opengl.GL11.GL_MODELVIEW;
import static org.lwjgl.opengl.GL11.GL_QUADS;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.GL_POLYGON;
import static org.lwjgl.opengl.GL11.GL_TRIANGLE_FAN;
import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glColor3f;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glLoadIdentity;
import static org.lwjgl.opengl.GL11.glMatrixMode;
import static org.lwjgl.opengl.GL11.glPolygonMode;
import static org.lwjgl.opengl.GL11.glPopMatrix;
import static org.lwjgl.opengl.GL11.glPushMatrix;
import static org.lwjgl.opengl.GL11.glRotated;
import static org.lwjgl.opengl.GL11.glScaled;
import static org.lwjgl.opengl.GL11.glTexCoord2d;
import static org.lwjgl.opengl.GL11.glTranslated;
import static org.lwjgl.opengl.GL11.glVertex2d;
import static org.lwjgl.opengl.GL11.glVertex3d;
import static org.lwjgl.opengl.GL11.glBindTexture;
import static org.lwjgl.opengl.GL11.glColor4b;
import static org.lwjgl.opengl.GL11.glColor3d;
import static org.lwjgl.opengl.GL11.glColor4d;

import java.awt.Point;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.lwjgl.opengl.Display;
import org.lwjgl.util.Color;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.font.effects.ColorEffect;

public class Graphics
{
	private static Graphics g;
	private TextureLoader loader;
	private UnicodeFont font;
	private DecimalFormat f = new DecimalFormat("#.##");
	private boolean toggleCamera = true, isFillDrawn = false;
	private Color color;
	
	private Graphics()
	{
		//super(owner, "graphics");
		loader = new TextureLoader();
		initText();
		color = null;
	}
	
	public static Graphics getInstance()
	{
		if(g == null)
			g = new Graphics();
		return g;
	}
	
	public TextureLoader getTextureLoader()
	{
		return loader;
	}
	
	public void toggleCamera(boolean b)
	{
		toggleCamera = b;
	}
	
	public void initText()
	{
		
		try 
		{
			font = new UnicodeFont("fonts/04B03.TTF", 16, false, false);
			font.getEffects().add(new ColorEffect(java.awt.Color.WHITE));
			font.addAsciiGlyphs();
			font.loadGlyphs();
		} 
		catch (SlickException e) { e.printStackTrace(); }
		
	}
	/*
	private void offset()
	{
		glTranslated(c.getWorldCenter().x, c.getWorldCenter().y, 0);
		glRotated(c.getTransform().getRotation(), 0, 0, 1);
		//glScaled(c.z, c.z, 0);
	}
	
	private void offset(Vector2 pos, double angle, double z, double scale)
	{
		// arcane math
		Vector2 centerPos = new Vector2(c.getX() + Display.getWidth() / 2, c.getY() + Display.getHeight() / 2);
		double radius = Math.hypot(pos.x - centerPos.x, pos.y - centerPos.y);
		double theta = Math.atan2(pos.y - centerPos.y, pos.x - centerPos.x) - Math.toRadians(c.getTransform().getRotation());
		Vector2 screenPos = new Vector2(0, 0);
		screenPos.x = ((pos.x - c.getX()) + (z * radius * Math.cos(theta) - (pos.x - centerPos.x)));
		screenPos.y = ((pos.y - c.getY()) + (z * radius * Math.sin(theta) - (pos.y - centerPos.y)));
		double screenAngle = angle - c.getTransform().getRotation();
		
		glTranslated(screenPos.x, screenPos.y, 0);
		glRotated(screenAngle, 0, 0, 1);
		glScaled(scale, scale, 0);
		glScaled(z, z, 0);
	}
	*/
	public void applyEffects()
	{
		if(color != null)
		{
			glColor4d(color.getRed() / 255.0, color.getGreen() / 255.0, color.getBlue() / 255.0, color.getAlpha() / 255.0);
		}
		else
		{
			glColor4d(1, 1, 1, 1);
		}
		
		if(isFillDrawn)
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		else
			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	}
	/*
	public void draw(Body b)
	{
		ArrayList<BodyFixture> fixtures = new ArrayList<BodyFixture>();
		for(int i = 0; i < b.getFixtureCount(); i++)
		{
			fixtures.add(b.getFixture(i));
		}
		
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glPushMatrix();
		glBindTexture(GL_TEXTURE_2D, 0);
		applyEffects();
		
		for(BodyFixture fixture : fixtures)
		{
			Convex convex = fixture.getShape();
			
			if(convex instanceof Shape)
			{
				
				offset(b.getWorldCenter(), Math.toDegrees(b.getTransform().getRotation()), 1, 1);
				if(convex instanceof Polygon)
				{
					Polygon p = (Polygon)convex;
					glBegin(GL_POLYGON);
					for(Vector2 v : p.getVertices())
						glVertex3d(v.x, v.y, 0.0);
					glEnd();
				}
				else if(convex instanceof Circle)
				{
					Circle c = (Circle)convex;
					double r = c.getRadius();
					glBegin(GL_TRIANGLE_FAN);
			        glVertex2d(0, 0);
			        int segments = 10;
			        for(int i = 0; i <= segments; i++) 
			        {
			            double t = 2 * Math.PI * i / segments;
			            glVertex2d(0 + Math.sin(t)*r, 0 + Math.cos(t)*r);
			        }
			        glEnd();
				}
			}
		}
		glPopMatrix();
	}
	*/
	/*
	public void drawSprite(Sprite s)
	{
		Texture t = s.getTexture();
		if(t != null)
		{
			double width = t.getWidth();
			double height = t.getHeight();
			glMatrixMode(GL_MODELVIEW);
			glLoadIdentity();
			glPushMatrix();
			offset(s.getWorldCenter(), s.getTransform().getRotation(), s.getZ(), s.getScale());
			glTranslated(-t.getImageWidth()/2, -t.getImageHeight()/2, 0);
			t.bind();
			glBegin(GL_QUADS);
				glTexCoord2d(0, 0);
				glVertex2d(0, 0); // upper-left
				
				glTexCoord2d(0, height);
				glVertex2d(0, t.getImageHeight()); // upper-right
				
				glTexCoord2d(width, height);
				glVertex2d(t.getImageWidth(), t.getImageHeight()); // bottom-right
				
				glTexCoord2d(width, 0);
				glVertex2d(t.getImageWidth(), 0); // bottom-left
			glEnd();
			glPopMatrix();
		}
	}
	*/
	public void drawPoint(double x, double y)
	{
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glPushMatrix();
		glBindTexture(GL_TEXTURE_2D, 0);
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		glBegin(GL_QUADS);
			glVertex2d(x - 2, y - 2); // upper-left
			glVertex2d(x + 2, y - 2); // upper-right
			glVertex2d(x + 2, y + 2); // bottom-right
			glVertex2d(x - 2, y + 2); // bottom-left
		glEnd();
		glPopMatrix();
	}
	
	public void drawQuad(double x, double y, double width, double height)
	{
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glPushMatrix();
		//offset();
		glBindTexture(GL_TEXTURE_2D, 0);
		applyEffects();
		glBegin(GL_QUADS);
			glVertex2d(x,         y);          // upper-left
			glVertex2d(x + width, y);          // upper-right
			glVertex2d(x + width, y + height); // bottom-right
			glVertex2d(x,         y + height); // bottom-left
		glEnd();
		glPopMatrix();
	}
	
	public void drawLine(Point v1, Point v2)
	{
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glPushMatrix();
		//offset();
		glBindTexture(GL_TEXTURE_2D, 0);
		applyEffects();
		glBegin(GL_LINES);
			glVertex2d(v1.x, v1.y);
			glVertex2d(v2.x, v2.y);
		glEnd();
		glPopMatrix();
	}
	
	public void drawTexture(Point pos, double angle, double z, double scale, Texture t)
	{
		if(t != null)
		{
			//System.out.println("texture drawing");
			double width = t.getWidth();
			double height = t.getHeight();
			glMatrixMode(GL_MODELVIEW);
			glLoadIdentity();
			glPushMatrix();
			//offset(pos, angle, z, scale);
			glTranslated(pos.x, pos.y, 0);
			//glTranslated(-t.getImageWidth()/2, -t.getImageHeight()/2, 0);
			applyEffects();
			t.bind();
			glBegin(GL_QUADS);
				glTexCoord2d(0, 0);
				glVertex2d(0, 0); // upper-left
				
				glTexCoord2d(0, height);
				glVertex2d(0, t.getImageHeight()); // upper-right
				
				glTexCoord2d(width, height);
				glVertex2d(t.getImageWidth(), t.getImageHeight()); // bottom-right
				
				glTexCoord2d(width, 0);
				glVertex2d(t.getImageWidth(), 0); // bottom-left
			glEnd();
			glPopMatrix();
		}
	}
	
	public void drawString(Point pos, double angle, boolean centered, String str)
	{
		setFillDrawn(true);
		applyEffects();
		if(centered)
			pos = new Point(pos.x - font.getWidth(str)/2, pos.y - font.getHeight(str)/2);
		
		if(color != null)
			font.drawString((float)pos.x, (float)pos.y, str, new org.newdawn.slick.Color(color.getRed(), color.getGreen(), color.getBlue()));
		else
			font.drawString((float)pos.x, (float)pos.y, str);
	}
	
	public String format(double d)
	{
		return f.format(d);
	}
	
	public UnicodeFont getFont()
	{
		return font;
	}
	
	public void setColor(Color color)
	{
		this.color = color;
	}
	
	public void setFillDrawn(boolean isFillDrawn)
	{
		this.isFillDrawn = isFillDrawn;
	}
}
