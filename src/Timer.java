

public class Timer {
	private double time;
	private double total;
	private boolean done, paused;
	
	public Timer(double time) {
		this.time = time;
		this.total = 0;
		done = false;
		paused = true;
	}
	
	public void start() {
		paused = false;
	}
	
	public void pause() {
		paused = true;
	}
	
	public void stop() {
		done = true;
	}
	
	public void reset() {
		done = false;
		total = 0;
	}
	
	public void update(double deltaTime) {
		if(!paused && !done) {
			total += deltaTime;
			if(total >= time) {
				total = time;
				done = true;
			}
		}
	}
	
	public void addTicks(double ticks) {
		this.time += ticks;
	}
	
	public void setTicks(double ticks) {
		this.time = ticks;
	}
	
	public double getTicks() {
		return time;
	}
	
	public void setRemaining(double remaining) {
		total = time - remaining;
	}
	
	public double getRemaining() {
		return time - total;
	}
	
	public boolean isDone() {
		return done;
	}
	
	public boolean isPaused() {
		return paused;
	}
	
	public double percent() {
		if(time == 0)
			return 0;
		return total / time;
	}
}
